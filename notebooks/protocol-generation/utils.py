# variables to copy from ssp370 according to AerChemMIP
SSP370_LOWNTCF_AERCHEMMIP_REFERENCE_VARIABLES = [
    "*CO2*",
    "*CH4*",
    "*N2O*",
    "*F-Gases*",
    "*Montreal Gases*",
]