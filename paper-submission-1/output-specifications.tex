RCMIP Phase 1's submission template (see \url{rcmip.org} or \url{https://doi.org/10.5281/zenodo.3593570}) is composed of two parts.
The first part is the data submission and is identical to the input format (see Section \ref{ssub:input_format}).
This allows for simplified analysis with the same tools we used to develop the input protocols and exchange with the IAMC community as they can analyse the data using existing tools such as pyam \citep{Gidden_2019_ggfx68}.
The second part is model metadata.
This includes the model's name, version number, brief description, literature reference and other diagnostics (see Section \ref{sub:diagnostics}).
We also request a configuration label, which uniquely identifies the configuration in which the model was run to produce the given results.

Given the typical temporal resolution of RCMs, we request all output be reported with an annual timestep.
In addition, to facilitate use of the output, participating modelling groups agree to have their submitted data made available under a Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license.
All input and output data, as well as all code required to produce this paper, is available at \url{gitlab.com/rcmip/rcmip} and archived at \url{https://doi.org/10.5281/zenodo.3593569}.

\subsection{Variables} % (fold)
\label{sub:variables}

RCMIP has a large variable request (26 Tier 1 variables, 344 Tier 2 variables and 13 Tier 3 variables), reflecting the large number of climate components included in RCMs.
Here we discuss the Tier 1 variables.
Tier 2 and 3 variables, which go into more detail for various parts of the climate system, are described in Supplementary Table \ref{tab:rcmip-variable-overview}.

The Tier 1 variables focus on key steps in the cause-effect chain from emissions to warming.
We request emissions of black carbon, \chem{CH_4}, carbon monoxide, \chem{CO_2}, \chem{N_2O}, \chem{NH_3}, nitrous oxides, organic carbon, sulphates and non-methane volatile organic compounds.
These cover the major greenhouse gases plus aerosol pre-cursor emissions.
In the case of emissions driven runs, these emissions are prescribed hence we only request that these variables are reported as outputs where the modelling groups have had to alter them (e.g. their model includes internal land-use calculations which cannot be exogenously overridden).
In the case of concentration-driven runs, we request emissions compatible with the prescribed concentration pathway (where these can be derived).
We also request cumulative emissions of \chem{CO_2} given their strong relationship with peak warming \citep{allen2009warming,matthews2009proportionality,Meinshausen_2009_b9j8fj,zickfeld2009setting}.

In Tier 1, we only request atmospheric concentrations of \chem{CO_2} and \chem{CH_4}.
Many models are capable of reporting much more detail than this, and we encourage them to report this detail, however some models only focus on a limited set of concentrations hence we restrict our Tier 1 variables.

In addition to concentrations, we request total, anthropogenic, \chem{CO_2} and aerosol effective radiative forcing and radiative forcing.
These forcing variables are key indicators of the long-term drivers of climate change within each model as well as being a key metric for the IAMC community.
Effective radiative forcing and radiative forcing are defined following \citet{IPCC_2013_WGI_Ch_8}.
In contrast to radiative forcing, effective radiative forcing includes rapid adjustments beyond stratospheric temperature adjustments thus is a better indicator of long-term climate change.

Finally in Tier 1, we request output of total climate system heat uptake, ocean heat uptake, surface air temperature change and surface ocean temperature change.
These variables are most directly comparable to available observations and CMIP output, with surface temperature also being highly policy-relevant.
Focusing on these key variables allows us to discern major differences between RCMs, with Tier 2 and 3 variables then providing further points of comparison at a finer level of detail.

\subsubsection{Probabilistic outputs} % (fold)
\label{ssub:probabilistic_outputs}

To reduce the total data volume, we request that groups provide only a limited set of percentiles from reporting probabilistic outputs, rather than every run which makes up the probabilistic ensemble.
The 10th, 50th (median) and 90th percentiles are Tier 1, with the 5th, 17th, 33rd, 67th, 83rd and 95th percentiles being Tier 2.
When calculating these percentiles, groups must take care to calculate derived quantities (e.g. Effective Climate Sensitivity) from each run in the probabilistic ensemble first and then calculate the percentiles in a second step.
Doing the reverse (calculating percentiles first, then derived quantities from percentiles) will not necessarily lead to the same answer.

% subsubsection probabilistic_outputs (end)

% subsection variables (end)

\subsection{Diagnostics} % (fold)
\label{sub:diagnostics}

On top of the variable request, we ask for one other diagnostic.
This is the equilibrium climate sensitivity, defined as `the equilibrium warming following an instantaneous doubling of atmospheric \chem{CO_2} concentrations'.
Unlike more complex models, RCMs typically have analytically tractable equilibrium climate sensitivities.
This means we do not need to include ten thousand year long simulations, which would allow the models to reach true equilibrium.
In contrast to the equilibrium climate sensitivity, the more commonly used effective climate sensitivity, derived using the Gregory method \citep{Gregory_2004_bm82st}, underestimates warming at true equilibrium in many models \citep{Rohrschneider_2019_ggfx33}.

% subsection diagnostics (end)
