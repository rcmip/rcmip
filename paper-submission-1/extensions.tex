RCMIP Phase 1 provides proof of concept of the RCMIP approach to RCM evaluation, comparison and examination.
The RCMIP Phase 1 protocol focuses on model evaluation hence is limited to experiments which are directly comparable to observations and CMIP output.
In this section we present a number of ways in which further research and phases of RCMIP could build on the work presented in this paper.

The first is a deeper evaluation of the results submitted to RCMIP Phase 1.
Here we have only presented illustrative results, however these can be evaluated and investigated in far more detail.
For example, quantifying the degree to which different RCMs agree with observations, carefully considering how to handle observational uncertainties, natural variability (which many RCMs cannot capture) and model tuning.

Secondly, there is a wide range of RCMs available in the literature.
This variety can be confusing, especially to those who are not intimately involved in developing the models.
An overview of the different models, their structure and relationship to one another would help reduce the confusion and provide clarity about the implications of using one model over another.

The third suggested extension is an investigation into how different RCMs reach equilibrium in response to a step change in forcing.
In RCMIP Phase 1, we only specified the equilibrium climate sensitivity value but temperature response is potentially further defined by linear and nonlinear feedbacks on different timescales.
Further phases could investigate whether model structure is a driver of difference between model output or whether these differences are largely controlled by differences in parameter values.

Fourthly, emulation results have generally only been submitted for a limited set of experiments (see Supplementary Table \ref{tab:emulation-scores-full} and Supplementary Figures \ref{fig:emulation-comparison-example-CanESM5-r1i1p2f1} - \ref{fig:emulation-comparison-example-CNRM-CM6-1-r1i1p1f2}).
Hence it is still not clear whether the emulation performance seen in idealised experiments also carries over to scenarios, particularly the SSP-based scenarios.
As the number of available CMIP6 results continues to grow, this area is ripe for investigation and will lead to improved understanding of the limits of the reduced complexity approach.
A common resource for RCM calibration would greatly aid this effort because CMIP6 data handling requires specialist big data handling skills.

Fifthly, while RCMIP Phase 1 allows us to evaluate the differences between RCMs, the root causes of these differences may not be clear.
This can be addressed by extending RCMIP to include experiments which specifically diagnose the reasons for differences between models e.g. simple pulse emissions of different species or prescribed step changes in atmospheric greenhouse gas concentrations.
Such experiments could build on existing research \citep{Vuuren_2009_bcmqjh,Schwarber_2019_ggfp6r} and would allow even more comprehensive examination and understanding of RCM behaviour.

Following this, there is clearly some variation in probabilistic projections.
However, what is not yet known is the extent to which variations in model structure, calibration data and calibration technique drive such differences.
Investigating these questions would help understand the limits of probabilistic projections and their uncertainties.
Experiments could involve constraining two different models with the same constraining technique and data, constraining a single model with two different techniques but the same data or constraining a single model with a single technique but two different datasets.

Next, the current experiments can be extended to examine the behaviour of models' gas cycles, particularly their interactions and feedbacks with other components of the climate system.
This will require custom experiments but is important for understanding the behaviour of these emissions driven runs.
Such experiments are particularly important for the carbon cycle, which is strongly coupled to other parts of the climate system.
It should be noted that, for ESMs, the suggestion of extra experiments is limited by human and  computational constraints.
This constraint does not apply to RCMs because of their computational efficiency: adding extra RCM experiments adds relatively little technical burden.

One final suggestion for future research is the importance of the choice of reference period.
Within the reference period, all model results and observations will be artificially brought together, narrowing uncertainty and disagreement within this period \citep{Hawkins_2016_f8x9vb}.
This can alter conclusions as the reference period will become less important for any fitting algorithm (because of the artificial agreement), placing more weight on other periods.
Developing a method to rebase both the mean and variance of model and observational results onto other reference periods would allow the impact of the reference period choice to be explored in a more systematic fashion.
