In the RCMIP community call (available at rcmip.org) RCMs were broadly defined as follows: ``[...] RCMIP is aimed at reduced complexity, simple climate models and small emulators that are not part of the intermediate complexity EMIC or complex GCM/ESM categories.''
In practice, we encouraged and encourage any group in the scientific community who identifies with the label of RCM to participate in RCMIP, see Table \ref{tab:rcmip-model-overview} for an overview of the models which participated in RCMIP Phase 1.

RCMIP Phase 1 focuses on evaluation of RCMs.
Specifically, comparing them against observations of the Earth System and the output of more complex models from CMIP5 and CMIP6 within two scientific themes.

\textbf{Theme 1: To what extent can reduced complexity models reproduce observed ranges of key climate change indicators (e.g. surface warming, ocean heat uptake, land carbon uptake)?}

The first theme focuses on evaluating models against observations.
Before using any model, one important question to ask is whether it can reproduce observations of the climate's recent evolution.
For RCMs, the key observation is changes in air and ocean temperatures \citep{Morice_2012_q4f,Cowtan_Way_2014}.
Beyond this, RCMs should also be evaluated against observed changes in ocean heat uptake \citep{Zanna_2019,von_schuckmann_2020} and estimates of carbon content in the air, land and oceans \citep{Friedlingstein_2019}.

These comparisons evaluate the extent to which the model's approximations cause its response to deviate from observational data.
However, most RCMs can be calibrated, i.e. have their parameters adjusted, such that they reproduce our best-estimate (typically median) observations.
Hence, where available, we also evaluate the extent to which RCMs can be configured to reproduce the range of available observational estimates too.
The handling of such observational estimates, particularly their uncertainties, is a complex topic in and of itself.
In RCMIP we rely on published estimates and make basic assumptions about how their uncertainty estimates should be compared to model output ranges, each of which we detail when the comparison is performed.

Given the limited amount of observations available and the ease of calibration of RCMs, comparing only with observations leaves us with little understanding of how RCMs perform in scenarios apart from a historic one in which anthropogenic emissions are heating the climate.
Recognising that there are a range of possible futures, it is vital to also assess RCMs in other scenarios.
Prominent examples include stabilising or falling anthropogenic emissions, strong mitigation of non-\chem{CO_2} climate forcers and scenarios with \chem{CO_2} removal.
The limited observational set motivates RCMIP's second theme: evaluation against more complex models.

\textbf{Theme 2: To what extent can reduced complexity models emulate the response of more complex models?}

Whilst the response of more comprehensive models may not represent the behaviour of the actual Earth System, they are the best available representation of the Earth System's physical processes.
By evaluating RCMs against more complex models, we can quantify the extent to which the simplifications made in RCMs limit their ability to capture physically-based model responses.
For example, the extent to which the approximation of a constant climate feedback limits an RCM's ability to replicate ESMs' longer-term response under either higher forcing or lower overshoot scenarios \citep{Rohrschneider_2019_ggfx33}.

In combination, these two research themes examine how well the reduced complexity approach can a) reproduce historical observations of the climate and b) respond to scenarios other than the recent past in a way which is consistent with our best understanding of the Earth system's physical and biogeochemical processes.
