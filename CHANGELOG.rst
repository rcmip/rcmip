Changelog
=========

master
------

- (`!50 <https://gitlab.com/rcmip/rcmip/merge_requests/50>`_) Verify that the ETag values for the files uploaded to s3 match the md5sum of the local files
- (`!49 <https://gitlab.com/rcmip/rcmip/merge_requests/49>`_) Release version v5.1.0 of the protocol
- (`!40 <https://gitlab.com/rcmip/rcmip/merge_requests/40>`_) Update Hector submission.
- (`!39 <https://gitlab.com/rcmip/rcmip/merge_requests/39>`_) Add UMD-EMGC phase 1 data submission. The v1-0-1 submission includes minor bug fixes:

    - Deleting spurious cells in ``your_data`` sheet
    - Renaming 'Historical' to 'historical'
    - Renaming 'Global' to 'World'

- (`!34 <https://gitlab.com/rcmip/rcmip/merge_requests/34>`_) Added ``rcmip`` package to repository

v4.0.0
------

- (`Manual merge <https://gitlab.com/rcmip/rcmip/-/commit/e532c82f67d45c5a2562d72c0d90847e55021903>`_) Submission to GMD. Also fixes:

    - AR5IR and Held two-layer submissions which had extra data columns (in v1-0-1)
    - FaIR effective climate feedback units by changing from ``K/W/m^2`` to ``W/m^2/K`` (in v1-0-1)
    - new MCE submission (including update to effective climate feedback units by changing from ``K/W/m^2`` to ``W/m^2/K`` (in v3-0-0))
    - WASP effective climate feedback units by changing from ``K/W/m^2`` to ``W/m^2/K`` (in v1-0-2)
    - OSCAR effective climate feedback units by changing from ``K/W/m^2`` to ``W/m^2/K`` (in v1-0-1)
    - MAGICC7 submission (was erroneous)

- (`!32 <https://gitlab.com/rcmip/rcmip/merge_requests/32>`_) Add AR5IR and Held two-layer model phase 1 data submission. Also fixes units of ``Effective Climate Feedback`` by changing from ``K/W/m^2`` to ``W/m^2/K``.
- (`!28 <https://gitlab.com/rcmip/rcmip/merge_requests/28>`_) Add OSCAR phase 1 data submission. Also adds ``Carbon Pool|Other`` to the protocol.
- (`!31 <https://gitlab.com/rcmip/rcmip/merge_requests/31>`_) Update ssp370-lowNTCF specifications. There are now two scenarios, ``ssp370-lowNTCF-aerchemmip``, be consistent with `Collins et al. 2017 <https://www.geosci-model-dev.net/10/585/2017/gmd-10-585-2017.pdf>`_ and ``ssp370-lowNTCF-gidden``, consistent with `Gidden et al. 2019 <https://www.geosci-model-dev.net/12/1443/2019/>`_. ``ssp370-lowNTCF-aerchemmip`` uses:

    - 'reference' CH4 concentrations ('reference' here meaning ssp370)
    - 'reference' non-CH4 WMGHG concentrations
    - 'clean' aerosol emissions ('clean' here meaning following `Gidden et al. 2019 <https://www.geosci-model-dev.net/12/1443/2019/>`_)
    - 'clean' ozone precursor emissions (well, really ozone precursors except methane so CO, NO2, OH, and VOCs)
    - 'reference' CFC/HCFC concentrations (where CFC/HCFC means all ozone depleting substances except N2O and methane in AerChemMIP, see `AerChemMIP FAQs <https://docs.google.com/document/d/1IprvZ2H9XXtBTqnQfF4IK0FYgQardQJZ165k3IS8knQ/edit>`_)

- (`!23 <https://gitlab.com/rcmip/rcmip/merge_requests/23>`_) Add miscellaneous plots
- (`!24 <https://gitlab.com/rcmip/rcmip/merge_requests/24>`_) Update cicero phase 1 submission. The v3-0-1 submission includes minor bug fixes:

    - Rename ``hist`` to ``historical``

- (`!29 <https://gitlab.com/rcmip/rcmip/merge_requests/29>`_) Update hector phase 1 submission
- (`!30 <https://gitlab.com/rcmip/rcmip/merge_requests/30>`_) Update protocol writing to be Pymagicc 2.0.0b10 compatible
- (`!22 <https://gitlab.com/rcmip/rcmip/merge_requests/22>`_) Add FaIR phase 1 data submission
- (`!21 <https://gitlab.com/rcmip/rcmip/merge_requests/21>`_) Add GIR (Generalised Impulse Response) phase 1 data submission
- (`!19 <https://gitlab.com/rcmip/rcmip/merge_requests/19>`_) Add ACC2 phase 1 data submission. Also adds ``Ocean pH`` to the RCMIP protocol. The v2-0-1 submission includes minor bug fixes:

    - Deleting spurious cells in ``your_data`` sheet

- (`!20 <https://gitlab.com/rcmip/rcmip/merge_requests/20>`_) Update GREB phase 1 data submission to use intended piControl runs
- (`!18 <https://gitlab.com/rcmip/rcmip/merge_requests/18>`_) Add WASP phase 1 data submission. v1-0-1 includes minor bug fixes:

        - Filled ``Model`` column with ``unspecified``

- (`!17 <https://gitlab.com/rcmip/rcmip/merge_requests/17>`_) Add MCE phase 1 data submission. v2-0-1 includes minor bug fixes:

    - Changing ``K/Mt CO2`` to ``K / MtCO2``

- (`!16 <https://gitlab.com/rcmip/rcmip/merge_requests/16>`_) Add Hector phase 1 data submission
- (`!15 <https://gitlab.com/rcmip/rcmip/merge_requests/15>`_) Add ESCIMO phase 1 data submission v2. v2-0-1 includes minor bug fixes:

    - Deleting spurious cells in ``your_data`` sheet

- (`!14 <https://gitlab.com/rcmip/rcmip/merge_requests/14>`_) Add CICERO-SCM phase 1 data submission. v1-0-1 includes minor bug fixes:

    - Rename ``esm-historical`` to ``esm-hist`` where it occurs

- (`!13 <https://gitlab.com/rcmip/rcmip/merge_requests/13>`_) Add GREB phase 1 data submission
- (`!12 <https://gitlab.com/rcmip/rcmip/merge_requests/12>`_) Add ESCIMO phase 1 data submission. v1-0-1 includes minor bug fixes:

    - Replacing ``ESCIMO,rcimp,base`` with ``ESCIMO,rcmip,base``
    - Deleting spurious cells in ``your_data`` sheet

- (`!11 <https://gitlab.com/rcmip/rcmip/merge_requests/11>`_) Add MAGICC7 phase 1 results
- (`!10 <https://gitlab.com/rcmip/rcmip/merge_requests/10>`_) Update dependencies and move all csvs out of git lfs to make changes easier to track
- (`!9 <https://gitlab.com/rcmip/rcmip/merge_requests/9>`_) Add continuous integration
- (`!8 <https://gitlab.com/rcmip/rcmip/merge_requests/8>`_) Add missing C5F12 timeseries

v3.1.0
------

- (`!7 <https://gitlab.com/rcmip/rcmip/merge_requests/7>`_) Update SSP ERFs to `v1.0 <https://github.com/Priestley-Centre/ssp_erf/tree/v1.0>`_
- (`!6 <https://gitlab.com/rcmip/rcmip/merge_requests/6>`_) Lengthen protocol for idealised experiments to avoid ambiguity

v3.0.0
------

- (`!5 <https://gitlab.com/rcmip/rcmip/merge_requests/5>`_) Make names consistent with CMIP6, clarify interpolation conventions, add SSP radiative forcings and clarify emissions driven experiments

    - Clarifies that any interpolation of the input protocol should be linear interpolation.
    - CMIP6 historical is now simply ``historical`` rather than ``historical-cmip6`` for consistency with CMIP6 output data.
    - Emissions driven experiments are now either ``esm-X`` or ``esm-X-allGHG``. ``esm-X`` have prescribed CO2 emissions and non-CO2 GHG concentrations to match CMIP6. ``esm-X-allGHG`` have prescribed GHG emissions hence do not match CMIP6 but are useful for other groups, particularly WG3. The descriptions of all experiments in the ``scenario_info`` sheet have been updated.

v2.0.0
------

- (`!4 <https://gitlab.com/rcmip/rcmip/merge_requests/4>`_) Add SSP consistent emissions and ZECMIP emissions to the protocol

    - Adds regional definitions to the protocol (they are in the ``region_definitions`` sheet of the data submission protocol)
    - For clarity, we also rename ``AFOLU`` to ``MAGICC AFOLU`` and ``Fossil and Industrial`` to ``MAGICC Fossil and Industrial``. ``MAGICC AFOLU`` is all agriculture, forestry and land-use change related emissions except fossil fuel combustion from agriculture. Hence it is not formally identical to IPCC WG3 AFOLU (see page 1304 of `AR5 WG3 Annex ii <https://www.ipcc.ch/site/assets/uploads/2018/02/ipcc_wg3_ar5_annex-ii.pdf>`_). Rather, it focusses on emissions which originate from the land carbon pool of the climate system rather than from fossil sources.

v1.0.1
------

- (`!3 <https://gitlab.com/rcmip/rcmip/merge_requests/3>`_) update protocol to clarify expectations and add ``Model`` column to ``your_data`` sheet of the submission template

    - While it is confusing, the ``Model`` column is required for compatibility with working group 3 data. As previously, the ``ClimateModel`` column should be used for specifying the model and its configuration. The ``Model`` column refers to the Integrated Assessment Model used to generate the data and so can be copied directly from the RCMIP input protocol.

- (`!2 <https://gitlab.com/rcmip/rcmip/merge_requests/2>`_) remove ``idx`` column from ``your_data`` sheet of the submission template

v1.0.0
------

- phase 1 release
