---- HEADER ----

Date: 2019-12-23 07:28:11
Contact: Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>, Jared Lewis <jared.lewis@climate-energy-college.org>, Malte Meinshausen <malte.meinshausen@unimelb.edu.au>
Source data crunched with: NetCDF-SCM v2.0.0-beta.5 (more info at gitlab.com/znicholls/netcdf-scm)
File written with: pymagicc v2.0.0-beta.10 (more info at github.com/openclimatedata/pymagicc)


---- METADATA ----

CMIP6_CV_version: cv=6.2.3.5-2-g63b123e
Conventions: CF-1.5
EXPID: ssp460
NCO: "4.6.0"
area_world (m**2): 510103740801024.0
area_world_el_nino_n3.4 (m**2): 5754542395392.0
area_world_land (m**2): 146545357881344.0
area_world_north_atlantic_ocean (m**2): 40768901443472.2
area_world_northern_hemisphere (m**2): 257873182912512.0
area_world_northern_hemisphere_land (m**2): 99463674835882.12
area_world_northern_hemisphere_ocean (m**2): 158409508079989.16
area_world_ocean (m**2): 363558415986851.9
area_world_southern_hemisphere (m**2): 252230557888512.0
area_world_southern_hemisphere_land (m**2): 47081690124819.85
area_world_southern_hemisphere_ocean (m**2): 205148867769512.03
branch_method: standard
branch_time_in_child: 0.0
branch_time_in_parent: 60265.0
calendar: gregorian
contact: ipsl-cmip6@listes.ipsl.fr
creation_date: 2019-01-30T10:41:04Z
crunch_contact: Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>, Jared Lewis <jared.lewis@climate-energy-college.org>, Malte Meinshausen <malte.meinshausen@unimelb.edu.au>
crunch_netcdf_scm_version: 2.0.0-beta.5 (more info at gitlab.com/znicholls/netcdf-scm)
crunch_source_files: Files: ['/CMIP6/ScenarioMIP/IPSL/IPSL-CM6A-LR/ssp460/r1i1p1f2/Amon/tas/gr/v20190307/tas_Amon_IPSL-CM6A-LR_ssp460_r1i1p1f2_gr_201501-210012.nc']; areacella: ['/CMIP6/ScenarioMIP/IPSL/IPSL-CM6A-LR/ssp460/r1i1p1f2/fx/areacella/gr/v20190307/areacella_fx_IPSL-CM6A-LR_ssp460_r1i1p1f2_gr.nc']; sftlf: ['/CMIP6/ScenarioMIP/IPSL/IPSL-CM6A-LR/ssp460/r1i1p1f2/fx/sftlf/gr/v20190307/sftlf_fx_IPSL-CM6A-LR_ssp460_r1i1p1f2_gr.nc']
data_specs_version: 01.00.28
description: near-surface (usually, 2 meter) air temperature
dr2xml_md5sum: c4b76079137f2c3b9298396d121b21c1
dr2xml_version: 1.16
experiment: update of RCP6.0 based on SSP4
experiment_id: ssp460
external_variables: areacella
forcing_index: 2
frequency: mon
further_info_url: https://furtherinfo.es-doc.org/CMIP6.IPSL.IPSL-CM6A-LR.ssp460.none.r1i1p1f2
grid: LMDZ grid
grid_label: gr
history: none
initialization_index: 1
institution: Institut Pierre Simon Laplace, Paris 75252, France
institution_id: IPSL
interval_operation: 900 s
interval_write: 1 month
land_fraction: 0.28728540130135394
land_fraction_northern_hemisphere: 0.38570770993906306
land_fraction_southern_hemisphere: 0.1866613249360149
license: CMIP6 model data produced by IPSL is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License (https://creativecommons.org/licenses). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file) and at https://cmc.ipsl.fr/. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
model_version: 6.1.8
name: /ccc/work/cont003/gencmip6/lurtont/IGCM_OUT/IPSLCM6/PROD/ssp460/CM61-LR-scen-ssp460/CMIP6/ATM/tas_Amon_IPSL-CM6A-LR_ssp460_r1i1p1f2_gr_%start_date%-%end_date%
nominal_resolution: 250 km
online_operation: average
parent_activity_id: CMIP
parent_experiment_id: historical
parent_mip_era: CMIP6
parent_source_id: IPSL-CM6A-LR
parent_time_units: days since 1850-01-01 00:00:00
parent_variant_label: r1i1p1f1
physics_index: 1
product: model-output
realization_index: 1
realm: atmos
source: IPSL-CM6A-LR (2017):  atmos: LMDZ (NPv6, N96; 144 x 143 longitude/latitude; 79 levels; top level 40000 m) land: ORCHIDEE (v2.0, Water/Carbon/Energy mode) ocean: NEMO-OPA (eORCA1.3, tripolar primarily 1deg; 362 x 332 longitude/latitude; 75 levels; top grid cell 0-2 m) ocnBgchem: NEMO-PISCES seaIce: NEMO-LIM3
source_id: IPSL-CM6A-LR
source_type: AOGCM BGC
sub_experiment: none
sub_experiment_id: none
table_id: Amon
title: IPSL-CM6A-LR model output prepared for CMIP6 / ScenarioMIP ssp460
tracking_id: hdl:21.14100/d63a50c4-79ec-4037-98b6-376665ddfc63
variable_id: tas
variant_info: Each member starts from the corresponding member of its parent experiment. Information provided by this attribute may in some cases be flawed. Users can find more comprehensive and up-to-date documentation via the further_info_url global attribute.
variant_label: r1i1p1f2

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 11
    THISFILE_DATAROWS = 86
    THISFILE_FIRSTYEAR = 2015
    THISFILE_LASTYEAR = 2100
    THISFILE_ANNUALSTEPS = 1
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'NONE'
    THISFILE_TIMESERIESTYPE = 'AVERAGE_YEAR_MID_YEAR'
    THISFILE_FIRSTDATAROW = 99
/

   VARIABLE                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas
       TODO                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET
      UNITS                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K
      YEARS               WORLD                 N34                LAND                 AMV                  NH              NHLAND             NHOCEAN               OCEAN                  SH              SHLAND             SHOCEAN
       2015         2.87188e+02         2.98747e+02         2.81095e+02         2.92077e+02         2.88064e+02         2.82814e+02         2.91361e+02         2.89644e+02         2.86292e+02         2.77464e+02         2.88318e+02
       2016         2.87270e+02         2.98599e+02         2.81204e+02         2.92476e+02         2.88122e+02         2.82837e+02         2.91440e+02         2.89715e+02         2.86399e+02         2.77753e+02         2.88383e+02
       2017         2.87213e+02         2.97433e+02         2.81097e+02         2.92021e+02         2.88113e+02         2.82768e+02         2.91469e+02         2.89678e+02         2.86292e+02         2.77566e+02         2.88295e+02
       2018         2.87417e+02         2.99432e+02         2.81359e+02         2.92229e+02         2.88365e+02         2.83088e+02         2.91678e+02         2.89859e+02         2.86448e+02         2.77705e+02         2.88454e+02
       2019         2.87298e+02         2.98265e+02         2.81345e+02         2.91910e+02         2.88184e+02         2.83000e+02         2.91439e+02         2.89697e+02         2.86392e+02         2.77848e+02         2.88352e+02
       2020         2.87384e+02         2.98442e+02         2.81378e+02         2.91921e+02         2.88245e+02         2.83033e+02         2.91518e+02         2.89804e+02         2.86503e+02         2.77881e+02         2.88481e+02
       2021         2.87499e+02         2.99538e+02         2.81583e+02         2.92225e+02         2.88447e+02         2.83305e+02         2.91676e+02         2.89884e+02         2.86529e+02         2.77945e+02         2.88499e+02
       2022         2.87662e+02         2.99617e+02         2.81716e+02         2.92603e+02         2.88613e+02         2.83419e+02         2.91874e+02         2.90058e+02         2.86689e+02         2.78118e+02         2.88656e+02
       2023         2.87607e+02         2.98366e+02         2.81658e+02         2.92427e+02         2.88500e+02         2.83337e+02         2.91742e+02         2.90005e+02         2.86695e+02         2.78111e+02         2.88664e+02
       2024         2.87573e+02         2.98445e+02         2.81465e+02         2.92397e+02         2.88316e+02         2.82979e+02         2.91667e+02         2.90036e+02         2.86815e+02         2.78266e+02         2.88777e+02
       2025         2.87741e+02         2.99208e+02         2.81724e+02         2.92547e+02         2.88699e+02         2.83527e+02         2.91946e+02         2.90167e+02         2.86762e+02         2.77914e+02         2.88793e+02
       2026         2.87633e+02         2.97686e+02         2.81772e+02         2.92574e+02         2.88669e+02         2.83637e+02         2.91828e+02         2.89996e+02         2.86575e+02         2.77831e+02         2.88582e+02
       2027         2.87459e+02         2.98311e+02         2.81316e+02         2.92255e+02         2.88394e+02         2.83036e+02         2.91758e+02         2.89935e+02         2.86503e+02         2.77682e+02         2.88527e+02
       2028         2.87800e+02         3.00106e+02         2.81873e+02         2.92339e+02         2.88807e+02         2.83622e+02         2.92062e+02         2.90189e+02         2.86770e+02         2.78176e+02         2.88742e+02
       2029         2.88011e+02         2.99472e+02         2.82221e+02         2.92854e+02         2.89084e+02         2.84003e+02         2.92274e+02         2.90345e+02         2.86915e+02         2.78456e+02         2.88856e+02
       2030         2.87779e+02         2.97958e+02         2.82015e+02         2.92506e+02         2.88774e+02         2.83809e+02         2.91891e+02         2.90102e+02         2.86762e+02         2.78225e+02         2.88722e+02
       2031         2.87800e+02         2.99060e+02         2.81992e+02         2.92381e+02         2.88857e+02         2.83839e+02         2.92008e+02         2.90141e+02         2.86719e+02         2.78089e+02         2.88700e+02
       2032         2.87957e+02         3.00086e+02         2.82137e+02         2.92645e+02         2.88955e+02         2.83815e+02         2.92183e+02         2.90303e+02         2.86937e+02         2.78594e+02         2.88851e+02
       2033         2.87976e+02         2.99357e+02         2.82225e+02         2.92495e+02         2.88887e+02         2.83871e+02         2.92037e+02         2.90294e+02         2.87044e+02         2.78748e+02         2.88948e+02
       2034         2.87724e+02         2.97663e+02         2.81814e+02         2.92358e+02         2.88653e+02         2.83509e+02         2.91884e+02         2.90107e+02         2.86774e+02         2.78233e+02         2.88735e+02
       2035         2.87811e+02         2.99292e+02         2.81860e+02         2.92339e+02         2.88778e+02         2.83629e+02         2.92011e+02         2.90209e+02         2.86822e+02         2.78123e+02         2.88818e+02
       2036         2.88134e+02         3.00311e+02         2.82286e+02         2.92757e+02         2.89089e+02         2.83946e+02         2.92319e+02         2.90491e+02         2.87157e+02         2.78781e+02         2.89080e+02
       2037         2.88222e+02         2.98918e+02         2.82404e+02         2.92904e+02         2.89136e+02         2.84072e+02         2.92316e+02         2.90566e+02         2.87287e+02         2.78880e+02         2.89216e+02
       2038         2.88087e+02         2.99262e+02         2.82153e+02         2.93119e+02         2.89157e+02         2.83991e+02         2.92401e+02         2.90478e+02         2.86992e+02         2.78271e+02         2.88994e+02
       2039         2.88219e+02         2.98760e+02         2.82521e+02         2.93116e+02         2.89341e+02         2.84357e+02         2.92470e+02         2.90515e+02         2.87072e+02         2.78642e+02         2.89006e+02
       2040         2.88170e+02         2.99562e+02         2.82337e+02         2.92847e+02         2.89301e+02         2.84200e+02         2.92504e+02         2.90521e+02         2.87013e+02         2.78402e+02         2.88989e+02
       2041         2.88415e+02         3.00714e+02         2.82779e+02         2.93030e+02         2.89562e+02         2.84601e+02         2.92677e+02         2.90686e+02         2.87242e+02         2.78929e+02         2.89149e+02
       2042         2.88383e+02         2.99318e+02         2.82797e+02         2.93082e+02         2.89604e+02         2.84721e+02         2.92669e+02         2.90635e+02         2.87136e+02         2.78732e+02         2.89064e+02
       2043         2.88239e+02         2.99787e+02         2.82423e+02         2.93113e+02         2.89427e+02         2.84413e+02         2.92576e+02         2.90584e+02         2.87025e+02         2.78219e+02         2.89046e+02
       2044         2.88460e+02         3.00736e+02         2.82753e+02         2.93334e+02         2.89634e+02         2.84623e+02         2.92780e+02         2.90760e+02         2.87260e+02         2.78803e+02         2.89200e+02
       2045         2.88341e+02         2.98371e+02         2.82791e+02         2.93207e+02         2.89453e+02         2.84509e+02         2.92557e+02         2.90579e+02         2.87205e+02         2.79162e+02         2.89051e+02
       2046         2.88286e+02         2.98445e+02         2.82611e+02         2.92999e+02         2.89449e+02         2.84452e+02         2.92587e+02         2.90574e+02         2.87097e+02         2.78723e+02         2.89019e+02
       2047         2.88491e+02         3.00000e+02         2.82840e+02         2.93173e+02         2.89632e+02         2.84685e+02         2.92738e+02         2.90768e+02         2.87324e+02         2.78943e+02         2.89247e+02
       2048         2.88711e+02         3.00583e+02         2.83195e+02         2.93286e+02         2.89845e+02         2.84990e+02         2.92894e+02         2.90934e+02         2.87551e+02         2.79404e+02         2.89420e+02
       2049         2.88515e+02         2.98533e+02         2.82920e+02         2.93264e+02         2.89704e+02         2.84818e+02         2.92772e+02         2.90771e+02         2.87300e+02         2.78911e+02         2.89226e+02
       2050         2.88452e+02         2.99485e+02         2.82700e+02         2.93124e+02         2.89604e+02         2.84471e+02         2.92828e+02         2.90770e+02         2.87273e+02         2.78961e+02         2.89181e+02
       2051         2.88552e+02         2.99458e+02         2.82968e+02         2.93315e+02         2.89754e+02         2.84766e+02         2.92886e+02         2.90803e+02         2.87323e+02         2.79168e+02         2.89195e+02
       2052         2.88783e+02         3.00949e+02         2.83214e+02         2.93479e+02         2.90018e+02         2.85151e+02         2.93075e+02         2.91028e+02         2.87521e+02         2.79121e+02         2.89448e+02
       2053         2.88924e+02         2.99911e+02         2.83361e+02         2.93898e+02         2.90189e+02         2.85262e+02         2.93282e+02         2.91166e+02         2.87631e+02         2.79346e+02         2.89532e+02
       2054         2.88848e+02         2.99989e+02         2.83347e+02         2.93491e+02         2.90070e+02         2.85189e+02         2.93135e+02         2.91066e+02         2.87599e+02         2.79457e+02         2.89468e+02
       2055         2.88811e+02         3.00156e+02         2.83367e+02         2.93556e+02         2.90031e+02         2.85150e+02         2.93096e+02         2.91005e+02         2.87563e+02         2.79599e+02         2.89391e+02
       2056         2.88871e+02         2.99526e+02         2.83568e+02         2.93519e+02         2.90101e+02         2.85444e+02         2.93025e+02         2.91009e+02         2.87614e+02         2.79607e+02         2.89452e+02
       2057         2.88793e+02         2.99777e+02         2.83300e+02         2.93281e+02         2.90034e+02         2.85260e+02         2.93032e+02         2.91007e+02         2.87523e+02         2.79159e+02         2.89443e+02
       2058         2.88920e+02         3.00893e+02         2.83401e+02         2.93439e+02         2.90131e+02         2.85280e+02         2.93177e+02         2.91145e+02         2.87682e+02         2.79431e+02         2.89576e+02
       2059         2.89027e+02         3.00071e+02         2.83612e+02         2.93523e+02         2.90239e+02         2.85413e+02         2.93270e+02         2.91210e+02         2.87788e+02         2.79806e+02         2.89620e+02
       2060         2.88935e+02         3.00515e+02         2.83187e+02         2.93502e+02         2.90081e+02         2.84969e+02         2.93290e+02         2.91252e+02         2.87765e+02         2.79423e+02         2.89679e+02
       2061         2.88881e+02         3.00279e+02         2.83363e+02         2.93580e+02         2.90047e+02         2.85151e+02         2.93121e+02         2.91105e+02         2.87689e+02         2.79584e+02         2.89549e+02
       2062         2.88981e+02         3.00295e+02         2.83565e+02         2.93671e+02         2.90287e+02         2.85489e+02         2.93299e+02         2.91164e+02         2.87645e+02         2.79499e+02         2.89515e+02
       2063         2.89006e+02         3.00073e+02         2.83544e+02         2.93749e+02         2.90234e+02         2.85406e+02         2.93265e+02         2.91208e+02         2.87751e+02         2.79610e+02         2.89619e+02
       2064         2.89151e+02         3.00698e+02         2.83687e+02         2.93709e+02         2.90374e+02         2.85488e+02         2.93441e+02         2.91354e+02         2.87902e+02         2.79882e+02         2.89742e+02
       2065         2.89168e+02         3.00254e+02         2.83752e+02         2.93672e+02         2.90482e+02         2.85637e+02         2.93525e+02         2.91350e+02         2.87824e+02         2.79772e+02         2.89671e+02
       2066         2.89194e+02         3.00217e+02         2.83781e+02         2.93939e+02         2.90533e+02         2.85697e+02         2.93570e+02         2.91375e+02         2.87824e+02         2.79733e+02         2.89681e+02
       2067         2.89137e+02         2.99964e+02         2.83739e+02         2.93622e+02         2.90538e+02         2.85804e+02         2.93510e+02         2.91312e+02         2.87704e+02         2.79377e+02         2.89615e+02
       2068         2.89228e+02         3.01164e+02         2.83852e+02         2.93499e+02         2.90540e+02         2.85800e+02         2.93515e+02         2.91395e+02         2.87887e+02         2.79735e+02         2.89758e+02
       2069         2.89325e+02         2.99690e+02         2.84123e+02         2.93804e+02         2.90677e+02         2.86056e+02         2.93579e+02         2.91422e+02         2.87943e+02         2.80039e+02         2.89757e+02
       2070         2.88996e+02         2.99198e+02         2.83488e+02         2.93740e+02         2.90312e+02         2.85484e+02         2.93343e+02         2.91217e+02         2.87652e+02         2.79273e+02         2.89575e+02
       2071         2.89349e+02         3.00442e+02         2.84048e+02         2.93738e+02         2.90684e+02         2.86033e+02         2.93604e+02         2.91485e+02         2.87983e+02         2.79855e+02         2.89849e+02
       2072         2.89360e+02         3.00767e+02         2.83923e+02         2.93723e+02         2.90818e+02         2.86106e+02         2.93777e+02         2.91552e+02         2.87870e+02         2.79310e+02         2.89834e+02
       2073         2.89602e+02         3.01348e+02         2.84372e+02         2.94031e+02         2.90982e+02         2.86222e+02         2.93971e+02         2.91710e+02         2.88191e+02         2.80464e+02         2.89964e+02
       2074         2.89720e+02         3.01302e+02         2.84564e+02         2.94285e+02         2.91165e+02         2.86528e+02         2.94076e+02         2.91798e+02         2.88242e+02         2.80414e+02         2.90039e+02
       2075         2.89798e+02         3.02520e+02         2.84680e+02         2.94514e+02         2.91250e+02         2.86570e+02         2.94189e+02         2.91862e+02         2.88314e+02         2.80687e+02         2.90064e+02
       2076         2.89714e+02         3.00123e+02         2.84453e+02         2.94350e+02         2.91090e+02         2.86392e+02         2.94040e+02         2.91835e+02         2.88307e+02         2.80356e+02         2.90132e+02
       2077         2.89520e+02         3.00118e+02         2.84241e+02         2.94077e+02         2.91044e+02         2.86326e+02         2.94006e+02         2.91648e+02         2.87963e+02         2.79835e+02         2.89828e+02
       2078         2.89563e+02         3.00495e+02         2.84379e+02         2.93860e+02         2.91010e+02         2.86356e+02         2.93932e+02         2.91653e+02         2.88084e+02         2.80204e+02         2.89893e+02
       2079         2.89417e+02         2.99776e+02         2.84044e+02         2.93913e+02         2.90816e+02         2.86026e+02         2.93824e+02         2.91583e+02         2.87988e+02         2.79857e+02         2.89854e+02
       2080         2.89425e+02         3.00047e+02         2.84014e+02         2.94038e+02         2.90769e+02         2.85916e+02         2.93816e+02         2.91605e+02         2.88050e+02         2.79996e+02         2.89898e+02
       2081         2.89663e+02         3.00732e+02         2.84410e+02         2.94158e+02         2.91129e+02         2.86515e+02         2.94027e+02         2.91781e+02         2.88164e+02         2.79964e+02         2.90046e+02
       2082         2.89662e+02         3.00165e+02         2.84447e+02         2.94257e+02         2.91185e+02         2.86484e+02         2.94137e+02         2.91764e+02         2.88104e+02         2.80144e+02         2.89931e+02
       2083         2.89720e+02         3.00400e+02         2.84589e+02         2.94103e+02         2.91268e+02         2.86756e+02         2.94102e+02         2.91788e+02         2.88137e+02         2.80010e+02         2.90002e+02
       2084         2.90036e+02         3.02168e+02         2.84850e+02         2.94475e+02         2.91574e+02         2.86903e+02         2.94507e+02         2.92127e+02         2.88465e+02         2.80514e+02         2.90290e+02
       2085         2.90077e+02         3.01692e+02         2.84853e+02         2.94576e+02         2.91601e+02         2.86789e+02         2.94622e+02         2.92183e+02         2.88520e+02         2.80762e+02         2.90300e+02
       2086         2.89969e+02         3.00503e+02         2.84902e+02         2.94329e+02         2.91492e+02         2.86957e+02         2.94339e+02         2.92011e+02         2.88411e+02         2.80561e+02         2.90213e+02
       2087         2.89939e+02         3.01004e+02         2.84721e+02         2.94371e+02         2.91478e+02         2.86822e+02         2.94401e+02         2.92042e+02         2.88365e+02         2.80281e+02         2.90220e+02
       2088         2.89944e+02         3.00372e+02         2.84762e+02         2.94151e+02         2.91424e+02         2.86736e+02         2.94367e+02         2.92033e+02         2.88431e+02         2.80592e+02         2.90230e+02
       2089         2.89898e+02         3.00803e+02         2.84662e+02         2.94103e+02         2.91396e+02         2.86671e+02         2.94363e+02         2.92009e+02         2.88367e+02         2.80417e+02         2.90191e+02
       2090         2.90194e+02         3.01659e+02         2.85219e+02         2.94419e+02         2.91775e+02         2.87295e+02         2.94588e+02         2.92199e+02         2.88576e+02         2.80834e+02         2.90353e+02
       2091         2.90086e+02         3.00298e+02         2.84963e+02         2.94434e+02         2.91615e+02         2.86969e+02         2.94533e+02         2.92151e+02         2.88523e+02         2.80725e+02         2.90312e+02
       2092         2.90082e+02         3.01121e+02         2.85003e+02         2.94392e+02         2.91606e+02         2.87031e+02         2.94479e+02         2.92129e+02         2.88523e+02         2.80721e+02         2.90313e+02
       2093         2.90313e+02         3.02353e+02         2.85276e+02         2.94721e+02         2.91885e+02         2.87319e+02         2.94753e+02         2.92343e+02         2.88705e+02         2.80962e+02         2.90482e+02
       2094         2.90277e+02         3.00337e+02         2.85267e+02         2.94642e+02         2.91798e+02         2.87246e+02         2.94656e+02         2.92296e+02         2.88721e+02         2.81086e+02         2.90473e+02
       2095         2.90235e+02         3.00346e+02         2.85159e+02         2.94573e+02         2.91815e+02         2.87211e+02         2.94707e+02         2.92280e+02         2.88618e+02         2.80824e+02         2.90407e+02
       2096         2.90360e+02         3.01718e+02         2.85324e+02         2.94583e+02         2.92088e+02         2.87563e+02         2.94929e+02         2.92391e+02         2.88594e+02         2.80593e+02         2.90431e+02
       2097         2.90405e+02         3.01544e+02         2.85369e+02         2.94710e+02         2.92061e+02         2.87414e+02         2.94979e+02         2.92434e+02         2.88712e+02         2.81051e+02         2.90470e+02
       2098         2.90522e+02         3.02483e+02         2.85544e+02         2.95038e+02         2.92318e+02         2.87797e+02         2.95157e+02         2.92529e+02         2.88686e+02         2.80783e+02         2.90500e+02
       2099         2.90593e+02         3.01420e+02         2.85867e+02         2.94930e+02         2.92364e+02         2.87973e+02         2.95121e+02         2.92498e+02         2.88783e+02         2.81417e+02         2.90474e+02
       2100         2.90518e+02         3.01805e+02         2.85613e+02         2.95116e+02         2.92344e+02         2.87904e+02         2.95132e+02         2.92496e+02         2.88652e+02         2.80774e+02         2.90460e+02
