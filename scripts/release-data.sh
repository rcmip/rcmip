#!/bin/bash
#
# Release a set of input data
#
# This should be done in lockstep with a release of the template protocols in https://gitlab.com/rcmip/pyrcmip to ensure that the
# versions are the same
#

ROOT_DIR=$(dirname "$0")/..
DATA_DIR=$ROOT_DIR/data/protocol
BUCKET=rcmip-protocols-au

VERSION=v5.1.0

aws configure set default.s3.multipart_threshold 128MB
aws configure set default.s3.multipart_chunksize 128MB

function upload {
  FNAME=$1

  echo "Uploading $FNAME as s3://$BUCKET/$VERSION/$FNAME"

  aws s3 cp $DATA_DIR/$FNAME s3://$BUCKET/$VERSION/$FNAME
  S3_MD5=$(aws s3api head-object --bucket $BUCKET --key ${VERSION}/${FNAME} | jq -r .ETag | tr -d '"')
  LOCAL_MD5=$(md5sum $DATA_DIR/$FNAME | awk '{ print $1 }')

  if [ "${LOCAL_MD5}" != "${S3_MD5}" ]
  then
      echo "md5sum Mismatch"
      echo "Local: ${LOCAL_MD5}"
      echo "S3: ${S3_MD5}"
      exit 1
  else
      echo "OK"
  fi
}

# data
upload rcmip-emissions-annual-means-${VERSION//./-}.csv
upload rcmip-concentrations-annual-means-${VERSION//./-}.csv
upload rcmip-radiative-forcing-annual-means-${VERSION//./-}.csv
