import glob
import os.path

SOURCE_DIR = os.path.join(".", "paper")

BASE_TEXT = """
\\begin{figure*}[t]
    \\includegraphics[width=12cm]{filename}
    \\caption[Emulation of modelname by RCMs]{Emulation of modelname by RCMs in RCMIP Phase 1. The thick transparent lines are the target CMIP6 model output (here from modelname). The thin lines are emulations from different RCMs. Panel (a) shows results for scenario based experiments while panels (b) - (e) show results for idealised CO2-only experiments (note that panels (b) - (e) share the same legend). }
    \\label{fig:emulation-comparison-example-modelcaption}
\\end{figure*}
"""

for f in glob.glob(os.path.join(SOURCE_DIR, "emulation-comparison*.pdf")):
    # print(f)
    model_no_escape = "-".join(f.split("-")[2:]).replace(".pdf", "")
    model = model_no_escape.replace("_", "\\_")
    modelcaption = model_no_escape.replace("_", "-")
    print(
        BASE_TEXT
        .replace("modelcaption", modelcaption)
        .replace("modelname", model)
        .replace("filename", os.path.basename(f))
    )
    print("\\clearpage")
