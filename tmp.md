Notes re minutes
----------------

- Get draft of protocol up asap
    - circulate to committee for silent adoption
    - key questions for committee
        - does this cover high priority experiments
            - Joeris' non-CO2
            - Piers' ERF
        - how is effective climate sensitivity over time defined?!
    - flags of what is going to have to come later
        - SSP emissions (most likely) which means all SSP experiments are on hold as you have to have aerosol emissions to do the runs
        - CMIP6 radiative forcings
    - document phase 1/phase 2 split for groups as clearly as possible
    - document configuration specs for groups as clearly as possible
    - decision re start point: we are focussing on emulation, start in 1850 (1750 start to phase 2)
    - document decision to not follow WG3 template exactly in a few key areas
        - we use 'radiative forcing', they use 'forcing' (more precise and clear for those of us for whom forcing means all sorts of things)
        - we use 'Surface Air Temperature Change' and 'Surface Ocean Temperature Change' etc., they use 'Temperature|Global Mean' (more precise)
        - we add in extra emissions subsector, 'Fossil and Industrial', which captures all the non-AFOLU variables as it's the fossil/afolu split we care about rather than industry split (groups can of course provide more emissions detail if this is relevant to them)
        - we use 'HFC4310', they use 'HFC43-10' (units handling libraries can't have hyphens in units)
        - we are currently not providing land cover information as we have no idea where to get it from, if a group in RCMIP needs and wants to lend their expertise that could be useful for others


- draft protocol extra tweaks
    - all experiments to be run in a 3K ECS setup as well as CMIP6 model specific calibrations
    - probabilistic setups wherever possible (percentiles only for now, get groups to save full output so asking for post-processing later is possible)
    - cut phase 1 as much as possible
    - For Phase-1 until 15th November:
        - Tier 1: Emission and concentration driven runs of SSP1-1.9 and SSP5-8.5. Also idealised 1pctCO2, abrupt4XCO2 and historical runs.
        - Tier 2: all other concentration driven SSP runs, plus impulse CDR experiments
        - Tier 3: all other emission driven SSP runs, plus all other experiments.


- todo
    - [x] variable sorting/final checks
    - [x] variable consistency with WG3
    - [x] change submission instructions to mention google drive
    - [x] sort out probabilistic submission conventions
        - [x] make 66th percentile 67th percentile
        - [x] make 87th percentile 83rd percentile
    - [x] scenario check
    - [x] specs re specifying model parameters
    - [ ] RCPs (consistent with variable definition)
    - [ ] add RCP scenarios
    - [ ] specify inputs as far as possible
    - [x] piControl experiments
    - [x] ssp370-lowNTCF experiment
    - [ ] extensions of all experiments
    - [ ] scenarios with different land-use assumptions
    - [ ] document start time conventions
    - [ ] document probabilistic variables to be added in phase 2/new output variables
    - [ ] Add ocean heat content data to request
    - [ ] Add blended temperatures data to request
    - [ ] Document that we want models to report inputs used in their runs too as a double check
    - [ ] Add effective climate feedback over time variable
    - [ ] Add Net Primary Productivity, vegetation carbon storage and soil carbon storage data to request
